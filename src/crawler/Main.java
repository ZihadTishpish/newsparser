package crawler;

import java.io.IOException;
import java.util.List;

public class Main {

    int monthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static void main(String gs[]) throws IOException {

        Fetcher fetcher = new Fetcher("2018/08/01");

        List<String> urls = fetcher.getFetchedLinks();

        for (String url : urls) {
            if (url.length() > 75) {
                System.out.println(fetcher.getTextData(url));
            }
        }
    }
}
