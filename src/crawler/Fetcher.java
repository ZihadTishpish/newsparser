package crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Fetcher {

    String dateUrl;

    Fetcher(String dateUrl){
        this.dateUrl = dateUrl;
    }

    public List<String> getFetchedLinks() throws IOException {
        Document doc = Jsoup.connect("https://www.jugantor.com/archive/"+dateUrl).get();
        Elements links = doc.select("a[href]");

        List<String> linkList = new ArrayList<>();
        for (Element link : links) {
            linkList.add(link.attr("abs:href"));
        }

        return linkList;
    }

    public String getTextData(String link) throws IOException {
        Document doc = Jsoup.connect(link).get();
        Elements texts = doc.select("p");

        StringBuilder sb = new StringBuilder();
        for (Element text : texts){
            sb.append(text.text());
        }

        return sb.toString();
    }
}
